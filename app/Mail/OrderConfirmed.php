<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use App\Order;

class OrderConfirmed extends Mailable
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Order $order)
    {
        return $this->view('emails.order_confirmed')
            ->with([
                'buyer_name' => $order->payer['']

            ]);
    }
}
