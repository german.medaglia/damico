<?php

namespace App\Listeners;

use App\Order;
use App\Events\OrderConfirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrder
{
    /**
     * Handle the event.
     *
     * @param  OrderConfirmed  $event
     * @return void
     */
    public function handle(OrderConfirmed $event)
    {
        $merchantOrder = $event->order;
        $order = new Order;
        $order->mp_merchant_id = $merchantOrder->id;
        $order->buyer_email = $merchantOrder->payer->email;
        $order->items = json_encode($merchantOrder->items);
        $order->status = $merchantOrder->status;
        $order->save();
    }
}