<?php

namespace App\Listeners;

use App\Events\PaymentApproved;
use App\Events\PaymentRefunded;
use App\Product;

class ProductStockUpdate
{
    /**
     * @param  OrderConfirmed  $event
     * @param  string          $paymentStatus
     * @return void
     */
    public function handle(MerchantOrder $order, string $paymentStatus)
    {
        $items = collect($order->items);
        $items->each(function ($item) use ($paymentStatus) {
            $product = Product::find($item->id);
            if ($product) {
                $product->stock = ($paymentStatus == 'refunded')
                    ? $product->stock + $item->quantity
                    : $product->stock - $item->quantit;
                $product->save();
            }
        });
    }


    public function onPaymentApproved(PaymentApproved $event)
    {
        $this->handle($event->order, 'approved');
    }

    public function onPaymentRefunded(PaymentRefunded $event)
    {
        $this->handle($event->order, 'refunded');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\PaymentApproved',
            'App\Listeners\ProductStockUpdate@onPaymentApproved'
        );
                
        $events->listen(
            'App\Events\PaymentRefunded',
            'App\Listeners\ProductStockUpdate@onPaymentRefunded'
        );
    }    
}