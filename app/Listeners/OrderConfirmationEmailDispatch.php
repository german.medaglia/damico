<?php

namespace App\Listeners;

use App\Events\OrderConfirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;
use App\Mail\OrderConfirmed as OrderConfirmedMail;

class OrderConfirmationEmailDispatch
{
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  OrderConfirmed  $event
     * @return void
     */
    public function handle(OrderConfirmed $event)
    {
        $order = $event->order;
        $this->mailer->to($order->payer['email'], new OrderConfirmedMail($order));
    }
}