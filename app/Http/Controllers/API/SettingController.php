<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use TCG\Voyager\Models\Setting;

class SettingController extends Controller
{

    public function index()
    {
        $settings = Setting::all();
        return response()->json($settings);
    }

}
