<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArtistCollection;
use App\Artist;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ArtistCollection
     */
    public function index()
    {
        $artists = Artist::ordered()->get();
        return new ArtistCollection($artists);
    }
}