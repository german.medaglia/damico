<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection;
use App\Category;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ProductCollection
     */
    public function index()
    {
        $products = Product::with('category')->ordered()->get();
        return new ProductCollection($products);
    }

    /**
     * Display a listing of the resource.
     *
     * @return ProductResource
     */
    public function show($slug)
    {
        $product = Product::with('videoLinks')->whereSlug($slug)->firstOrFail();
        return new ProductResource($product);
    }
}
