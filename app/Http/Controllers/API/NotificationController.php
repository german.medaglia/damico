<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use MercadoPago\SDK as MP;
use MercadoPago\MerchantOrder;
use MercadoPago\Payment;
use App\Http\Controllers\Controller;
use App\Events\PaymentApproved;
use App\Events\PaymentRefunded;

class NotificationController extends Controller
{
    public function index(Request $request)
   	{
	    $topic = $request->input('topic');

	    if ($topic != 'payment') {
	    	return response(null);
	    }

        MP::setAccessToken(config('mp.access_token'));

        $payment = Payment::find_by_id($request->input('id'));
	    $merchantOrder = MerchantOrder::find_by_id($payment->order->id);

	    switch ($payment->status) {
	    	case 'approved':
	    		event(new PaymentApproved($merchantOrder));
	    		break;
    		case 'refunded':
    			event(new PaymentRefunded($merchantOrder));
    			break;
	    }

	    return response(null);
   	} 
}
