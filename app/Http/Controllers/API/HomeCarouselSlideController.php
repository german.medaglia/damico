<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SlideCollection;
use App\HomeCarouselSlide;

class HomeCarouselSlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = HomeCarouselSlide::ordered()->get();
        return new SlideCollection($slides);
    }
}