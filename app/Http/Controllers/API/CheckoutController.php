<?php namespace App\Http\Controllers\API;

use App\Product;
use Voyager;
use MercadoPago\SDK as MP;
use Illuminate\Http\Request;
use MercadoPago\Preference;
use MercadoPago\Item;
use MercadoPago\Shipments;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;

class CheckoutController extends Controller
{

    public function index(Request $request)
    {
        $productId = $request->input('productId', null);
        $product = Product::findOrFail($productId);

        if ($product->stock == 0) {
            return response()->json([
                'error' => 'Lo sentimos, el producto solicitado ya no posee stock.'
            ], 488);
        }

        MP::setClientId(config('mp.client_id'));
        MP::setClientSecret(config('mp.client_secret'));

        $preference = new Preference;
        $preference->marketplace = 'damicoefectos.com';
        $item = new Item;
        $item->id = $product->id;
        $item->title = $product->name;
        $item->quantity = 1;
        $item->unit_price = floatval($product->price);
        $item->picture_url = asset(Voyager::image($product->firstThumbnail('sm')));
        $item->currency_id = 'ARS';
        $item->shipments = new Shipments;
        $item->shipments->mode = 'custom'; // 'not_specified';
        $item->shipments->dimensions = $product->dimensions;
        //$item->shipments->default_shipping_method = null;
        $preference->items = [$item];
        $productsRoute = route('products');
        $preference->back_urls = [
            'success' => $productsRoute,
            'failure' => $productsRoute,
            'pending' => $productsRoute
        ];
        $preference->notification_url = route('notifications') . '?source_news=ipn';

        $preference->save();

        return new JsonResource([
            'checkoutUrl' => $preference->{config('mp.response_init_point_attribute')}
        ]);
    }

}
