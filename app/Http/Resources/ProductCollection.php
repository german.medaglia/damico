<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request)
    {
        return [
            'metadata' => [
                'count' => $this->collection->count(),
                'categories' => $this->collection->unique('category_id')->values()->transform(function ($item) {
                    return $item->category;
                })
            ],
        ];
    }    
}
