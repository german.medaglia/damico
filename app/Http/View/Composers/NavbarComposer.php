<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;

class NavbarComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('brand_img', setting('site.logo'));
        $view->with('nav_items_sections', [
            ['label' => 'Nosotros', 'link' => route('home')],
            ['label' => 'Productos', 'link' => route('products')],
            ['label' => 'Artistas', 'link' => route('artists')],
            ['label' => 'Cómo comprar', 'link' => route('how-to-buy')],
        ]);

        $view->with('nav_items_social', [
            ['icon' => 'fa-facebook-square', 'link' => setting('site.facebook_profile_url')],
            ['icon' => 'fa-instagram', 'link' => setting('site.instagram_profile_url')],
        ]);
    }
}
