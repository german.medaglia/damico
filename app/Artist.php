<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Sortable;
use Voyager;

class Artist extends Model
{
	use Sortable;

    protected $table = 'artists';

    protected $hidden = ['order', 'image', 'created_at', 'updated_at'];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return Voyager::image($this->attributes['image']);
    }
}
