<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Sortable;

class Category extends Model
{
	use Sortable;
	
    protected $table = 'categories';

    //public $timestamps = false;

    protected $hidden = ['order', 'created_at', 'updated_at'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function scopeHavingProducts($query)
    {
    	return $query->whereHas('products');

    }

}
