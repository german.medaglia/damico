<?php

namespace App\Events;

use MercadoPago\MerchantOrder;

class PaymentEvent
{
    public $order;

    /**
     * Create a new event instance.
     *
     * @param  MerchantOrder  $order
     * @return void
     */
    public function __construct(MerchantOrder $order)
    {
        $this->order = $order;
    }
}