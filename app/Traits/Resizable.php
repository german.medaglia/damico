<?php
namespace App\Traits;

use TCG\Voyager\Traits\Resizable as VoyagerResizable;

trait Resizable
{
    use VoyagerResizable;

    /**
     * Method for returning specific thumbnail for model.
     *
     * @param string $type
     * @param string $attribute
     *
     * @return string
     */
    public function firstThumbnail($type, $attribute = 'images')
    {
        // Return empty string if the field not found
        if (!isset($this->attributes[$attribute])) {
            return null;
        }

        // We take image from posts field
        $images = json_decode($this->attributes[$attribute]);

        if (!empty($images)) {
            $image = $images[0];
            return $this->getThumbnail($image, $type);
        }

        return null;

    }

    /**
     * Method for returning thumbnails for model.
     *
     * @param string $type
     * @param string $attribute
     *
     * @return string
     */
    public function thumbnails($type, $attribute = 'images')
    {
        // Return empty string if the field not found
        if (!isset($this->attributes[$attribute])) {
            return null;
        }

        $images = json_decode($this->attributes[$attribute]);

        if (empty($images)) {
            return null;
        }

        return array_map(function ($image) use ($type) {
            return $this->getThumbnail($image, $type);
        }, $images);
    }
}
