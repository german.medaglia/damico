<?php
namespace App\Traits;

trait Sortable
{
    public function scopeOrdered($query)
    {
        return $query->orderBy('order');
    }
}
