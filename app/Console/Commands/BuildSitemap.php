<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Product;

class BuildSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $links = collect(['products', 'artists', 'how-to-buy'])->map(function ($item) {
            $link = route($item);
            $this->info($link);
            return $link;
        });

        $products = Product::all();

        $products->each(function ($product) use (&$links) {
            $link = route(
                'product',
                ['slug' => $product->slug]
            );
            $links->push($link);
            $this->info($link);
        });
        file_put_contents(public_path() . '/sitemap.txt', $links->join(PHP_EOL));
        $this->info(count($links) . ' links created.');
    }
}
