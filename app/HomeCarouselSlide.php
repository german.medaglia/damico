<?php
namespace App;

use App\Traits\Sortable;
use Illuminate\Database\Eloquent\Model;
use Voyager;

class HomeCarouselSlide extends Model
{
	use Sortable;

    protected $table = 'home_carousel_slides';

    protected $hidden = ['image', 'order', 'created_at', 'updated_at'];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return Voyager::image($this->attributes['image']);
    }

}
