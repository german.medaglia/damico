<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Resizable;
use App\Traits\Sortable;
use Voyager;

class Product extends Model
{
    use Resizable;
    use Sortable;

    protected $appends = ['image_urls', 'formatted_price'];

    protected $hidden = ['price', 'images', 'order', 'created_at', 'updated_at', 'category'];

    protected $casts = ['category_id' => 'integer', 'stock' => 'integer'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function videoLinks()
    {
        return $this->hasMany(VideoLink::class, 'product_id');
    }

    public function getImageUrlsAttribute()
    {
        $images = json_decode($this->attributes['images']);

        $fullImageUrls = array_map(function ($img) {
            return Voyager::image($img);
        }, $images ?: []);

        return [
            'full' => $fullImageUrls,
            'lg' => $this->getThumbs('lg'),
            'md' => $this->getThumbs('md'),
            'sm' => $this->getThumbs('sm')
        ];
    }

    public function getFormattedPriceAttribute()
    {
        return '$ ' . number_format($this->price, 0, ',', '');
    }

    public function setSlugAttribute()
    {
        $this->attributes['slug'] = str_slug($this->name, '-');
    }

    private function getThumbs(string $type)
    {
        $thumbs = $this->thumbnails($type);
        if (empty($thumbs)) {
            return [];
        }

        return array_map(function ($thumb) {
            return Voyager::image($thumb);
        }, $thumbs);
    }
}
