<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', function() {
    $slides = App\HomeCarouselSlide::orderBy('order')->get();
    return view('welcome', [
        'slides' => $slides
    ]);
}]);

Route::get('/productos', ['as' => 'products', function() {
    return view('products', [
    	'products' => App\Product::ordered()->get(),
    	'categories' => App\Category::havingProducts()->ordered()->get(),
    ]);
}]);

Route::get('/productos/{slug}', ['as' => 'product', function($slug) {
    return view('product', [
    	'product' => App\Product::whereSlug($slug)->with('videoLinks')->firstOrFail()
    ]);
}]);

Route::post('/checkout', 'CheckoutController@index')->name('checkout');

Route::get('/artistas', ['as' => 'artists', function() {
    return view('artists', [
    	'artists' => App\Artist::ordered()->get()
    ]);
}]);

Route::get('/como-comprar', ['as' => 'how-to-buy', function() {
    return view('how_to_buy');
}]);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
