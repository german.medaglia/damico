<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('products', 'API\ProductController')->only(['index', 'show']);
Route::apiResource('categories', 'API\CategoryController')->only('index');
Route::apiResource('artists', 'API\ArtistController')->only('index');
Route::apiResource('home-carousel-slides', 'API\HomeCarouselSlideController')->only('index');
Route::get('settings', 'API\SettingController@index');
Route::post('checkout', 'API\CheckoutController@index');
Route::post('notifications', ['uses' => 'API\NotificationController@index', 'as' => 'notifications']);
