<?php

return [
    'client_id' => env('MP_CLIENT_ID'),
    'client_secret' => env('MP_CLIENT_SECRET'),
    'access_token' => env('MP_ACCESS_TOKEN'),
    'response_init_point_attribute' => env('MP_RESPONSE_INIT_POINT_ATTRIBUTE')
];