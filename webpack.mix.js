const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var baseJsFiles = [
    "./node_modules/jquery/dist/jquery.min.js",
    "./node_modules/bootstrap-vue/dist/bootstrap-vue.min.js"
];

mix.sass('resources/assets/sass/app.scss', 'public/css/damico.css')
    .styles([
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        './node_modules/bootstrap-vue/dist/bootstrap-vue.min.css',
        './node_modules/@fortawesome/fontawesome-free/css/all.css'
    ], 'public/css/lib.css')
    .scripts(baseJsFiles, 'public/js/base.js')
    .js('resources/assets/js/app.js', 'public/js')
    .copyDirectory('./node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts');

