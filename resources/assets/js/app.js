
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('navbar', require('./components/NavbarComponent.vue'));
Vue.component('product-list', require('./components/ProductListComponent.vue'));
Vue.component('product-detail', require('./components/ProductDetailComponent.vue'));
Vue.component('img-gallery', require('./components/ImageGalleryComponent.vue'));
Vue.component('home', require('./components/HomeComponent.vue'));
Vue.component('artists', require('./components/ArtistsComponent.vue'));
Vue.component('how-to-buy', require('./components/HowToBuyComponent.vue'));
Vue.component('site-footer', require('./components/SiteFooterComponent.vue'));
Vue.component('buy-cta', require('./components/BuyCTAComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

import VueResource from 'vue-resource';

Vue.use(VueResource);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#page'
});
