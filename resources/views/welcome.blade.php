@extends('layouts.master')

@section('content')

    <home :carousel-slides="{{ $slides->toJson() }}" company-description="{{ setting('site.company_description') }}"></home>

@stop
