@extends('layouts.master')

@section('content')
    <artists :artists="{{ $artists->toJson() }}"></artists>
@stop
