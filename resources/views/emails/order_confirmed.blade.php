<h1>Gracias por tu compra, {{ $buyer_name}} !</h1>

<p>Tu orden de compra fue confirmada exitosamente. Recibirás tu pedido en los próximos días</p>

<h3>Detalle del pedido</h3>

<ul>
	@foreach ($item in $items):
		<li>{{ $item->title }} x{{ $item->quantity}}</li>
	@endforeach
</ul>