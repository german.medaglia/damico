@extends('layouts.master')

@section('jsonld')
    {
        "@context": "http://schema.org",
        "@type": "Product",
        "name": "{{ $product->name }}",
        "image": "{{ $product->image_urls['sm'][0] }}",
        "description" : "{{ $product->description }}",
        "brand" : {
            "@type": "Brand",
            "name": "D'Amico FX"
        },
        "offers" : {
            "@type": "Offer",
            "price": "{{ $product->price }}",
            "priceCurrency": "ARS",
            "url": "{{ url()->current() }}"
        }
    }
@stop

@section('content')
    <product-detail :product="{{ $product->toJson() }}"></product-detail>

    <div class="fb-comments mt-5" data-href="{{ url()->current() }}" data-width="100%" data-numposts="10"></div>
@stop
