<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ setting('site.google_analytics_tracking_id') }}"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', '{{ setting("site.google_analytics_tracking_id") }}');
        </script>         
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>@yield('title', 'D\'Amico FX - Equipamiento para músicos.')</title>
        <meta property="og:title" content="D'Amico FX" />
        <meta property="og:description" content="Equipamiento para músicos." />
        <meta property="og:url" content="{{ url()->full() }}" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="{{ \Voyager::image('logo_fb.jpg') }}" />
        <meta property="og:image:alt" content="D'Amico Efectos" />
        <meta property="og:locale" content="es_ES" />
        <meta property="fb:app_id" content="2219365248302124" />
        <link rel="icon" href="/img/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="." />
        <link media="all" type="text/css" rel="stylesheet" href="/css/lib.css" />
        <link media="all" type="text/css" rel="stylesheet" href="/css/damico.css" />

        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.typekit.net/aux2lqj.css">
        <script v-html="jsonld" type="application/ld+json">
            @yield('jsonld', '')
        </script>

    </head>
    <body>
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v5.0&appId=2219365248302124&autoLogAppEvents=1"></script>

        <div id="page">            
            @php
                $fb_profile_url = setting('site.facebook_profile_url');
                $ig_profile_url = setting('site.instagram_profile_url');
                $brand_img = \Voyager::image(setting('site.logo'));
                $contact_email = setting('site.contact_email');
            @endphp

            <header id="site-header" class="mb-4">
                <div class="container">
                    <navbar fb-profile-url="{{ $fb_profile_url }}" ig-profile-url="{{ $ig_profile_url }}" brand-img="{{ $brand_img }}"></navbar>
                </div>
            </header>

            <div class="container">
                <main id="content">
                    @yield('content')
                </main>

            </div>

            <site-footer contact-email="{{ $contact_email }}"></site-footer>

        </div>

        @push('scripts')
            {!! HTML::script('js/base.js') !!}
            {!! HTML::script('js/app.js') !!}
        @endpush
        @stack('scripts')
    </body>
</html>
